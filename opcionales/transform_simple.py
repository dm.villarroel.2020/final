import sys
from images import read_img, write_img
from transforms import rotate_right, mirror, blur, grayscale, sepia

def apply_transformation(file_name, transformation_func):
    # Lee la imagen del archivo
    pixels = read_img(file_name)

    # Aplica la función de transformación especificada
    if transformation_func == 'rotate_left':
        # Realiza la rotación de la imagen 90 grados a la izquierda
        transformed_pixels = rotate_right(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_rotated.png"
    elif transformation_func == 'mirror':
        # Realiza el espejado vertical de la imagen
        transformed_pixels = mirror(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_mirrored.png"

    elif transformation_func == 'blur':
        # Aplica el efecto de desenfoque
        transformed_pixels = blur(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_blurred.png"
        # print(f"Imagen con efecto de desenfoque guardada como {output_file}")

    elif transformation_func == 'grayscale':
        # Convierte la imagen a escala de grises
        transformed_pixels = grayscale(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_grayscale.png"

    elif transformation_func == 'sepia':
        # Transforma la imagen a sepia
        transformed_pixels = sepia(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_sepia.png"
        write_img(transformed_pixels, output_file)
        print(f"Imagen transformada a sepia guardada como {output_file}")
    else:
        print("Función de transformación no válida.")
        return

    # Escribe la imagen transformada en un nuevo archivo
    write_img(transformed_pixels, output_file)
    print(f"Imagen transformada guardada como {output_file}")

def main():
    # Verifica que se proporcionen suficientes argumentos
    if len(sys.argv) != 3:
        print("Uso: python transform_simple.py <nombre_imagen> <rotate_left|mirror>")
        return

    file_name = sys.argv[1]
    transformation_func = sys.argv[2]

    apply_transformation(file_name, transformation_func)

if __name__ == '__main__':
    main()
