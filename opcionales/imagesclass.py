from PIL import Image as PILImage

class Image:
    def __init__(self, file_path):
        self.pil_image = PILImage.open(file_path)
        self.width, self.height = self.pil_image.size
        self.pixels = self.load_pixels()

    def load_pixels(self):
        pixel_matrix = []
        for y in range(self.height):
            row = []
            for x in range(self.width):
                pixel = self.pil_image.getpixel((x, y))
                row.append(pixel)
            pixel_matrix.append(row)
        return pixel_matrix

    def get_pixel(self, x, y):
        return self.pixels[y][x]

    def set_pixel(self, x, y, color):
        self.pixels[y][x] = color

if __name__ == "__main__":
    img = Image("ruta/imagen.png")

