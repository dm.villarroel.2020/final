def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    """
    Cambia los colores de una imagen.

    Args:
    - image: Lista de listas que representa la imagen.
    - to_change: Color a cambiar representado como una tupla (R, G, B).
    - to_change_to: Nuevo color representado como una tupla (R, G, B).

    Returns:
    - La imagen con los colores cambiados.
    """
    changed_image = []
    for row in image:
        new_row = []
        for pixel in row:
            # print(f"Pixel antes del cambio: {pixel}")  # Imprimir el pixel antes del cambio
            if pixel == to_change:
                new_row.append(to_change_to)
            else:
                new_row.append(pixel)
            # print(f"Pixel después del cambio: {new_row[-1]}")  # Imprimir el pixel después del cambio
        changed_image.append(new_row)
    return changed_image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Gira una imagen 90 grados a la derecha.

    Args:
    - image: Lista de listas que representa la imagen.

    Returns:
    - La imagen girada 90 grados a la derecha.
    """
    # Obtiene las dimensiones de la imagen original
    rows = len(image)
    cols = len(image[0])

    # Crea una nueva matriz para la imagen girada
    rotated_image = [[(0, 0, 0) for _ in range(rows)] for _ in range(cols)]

    # Rota la imagen
    for i in range(rows):
        for j in range(cols):
            rotated_image[cols - 1 - j][i] = image[i][j]

    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Crea una imagen "espejada" verticalmente según un eje vertical en la mitad de la imagen.

    Args:
    - image: Lista de listas que representa la imagen.

    Returns:
    - La imagen espejada verticalmente.
    """
    # Obtiene las dimensiones de la imagen original
    rows = len(image)
    cols = len(image[0])

    # Crea una nueva matriz para la imagen espejada
    mirrored_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    # Espeja la imagen
    for i in range(rows):
        for j in range(cols):
            # Los píxeles se copian de izquierda a derecha en el lado opuesto del espejo vertical
            mirrored_image[i][j] = image[rows - 1 - i][j]

    return mirrored_image

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    """
    Cambia los colores de una imagen sumando o restando un incremento a cada componente RGB.

    Args:
    - image: Lista de listas que representa la imagen.
    - increment: Valor para incrementar o decrementar los componentes RGB.

    Returns:
    - La imagen con los colores cambiados según el incremento especificado.
    """
    # Crea una nueva imagen para almacenar los píxeles modificados
    rotated_colors_image = []

    # Itera sobre cada píxel de la imagen original
    for row in image:
        new_row = []
        for pixel in row:
            # Ajusta el incremento si el valor es negativo y alcanza 0
            adjusted_increment = increment
            if increment < 0:
                adjusted_increment = 256 + increment

            # Aplica el incremento a cada componente RGB del píxel
            new_pixel = tuple((component + adjusted_increment) % 256 for component in pixel)
            new_row.append(new_pixel)
        rotated_colors_image.append(new_row)

    return rotated_colors_image

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Crea una imagen con efecto de desenfoque, promediando los valores RGB de los píxeles adyacentes.

    Args:
    - image: Lista de listas que representa la imagen.

    Returns:
    - La imagen modificada con el efecto de desenfoque.
    """
    # Obtiene las dimensiones de la imagen original
    rows = len(image)
    cols = len(image[0])

    # Crea una nueva matriz para la imagen con efecto de desenfoque
    blurred_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    # Itera sobre cada píxel de la imagen original
    for i in range(rows):
        for j in range(cols):
            total_pixels = 0
            total_red = 0
            total_green = 0
            total_blue = 0

            # Calcula la suma de los valores RGB de los píxeles adyacentes válidos
            for x, y in [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1), (i - 1, j - 1), (i - 1, j + 1), (i + 1, j - 1),
                         (i + 1, j + 1)]:
                # Resto del código para calcular el promedio

                if 0 <= x < rows and 0 <= y < cols:
                    total_pixels += 1
                    pixel = image[x][y]
                    total_red += pixel[0]
                    total_green += pixel[1]
                    total_blue += pixel[2]

            # Calcula el valor promedio de los valores RGB
            if total_pixels > 0:
                averaged_red = total_red // total_pixels
                averaged_green = total_green // total_pixels
                averaged_blue = total_blue // total_pixels
                blurred_image[i][j] = (averaged_red, averaged_green, averaged_blue)

    return blurred_image

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    """
    Desplaza una imagen en la cantidad especificada de píxeles horizontal y verticalmente.

    Args:
    - image: Lista de listas que representa la imagen.
    - horizontal: Número de píxeles a desplazar horizontalmente (derecha si es positivo, izquierda si es negativo).
    - vertical: Número de píxeles a desplazar verticalmente (arriba si es positivo, abajo si es negativo).

    Returns:
    - La imagen modificada después del desplazamiento.
    """
    rows = len(image)
    cols = len(image[0])

    shifted_image = [[(0, 0, 0) for _ in range(cols)] for _ in range(rows)]

    for i in range(rows):
        for j in range(cols):
            new_i = i + vertical
            new_j = j + horizontal

            if 0 <= new_i < rows and 0 <= new_j < cols:
                shifted_image[new_i][new_j] = image[i][j]

    return shifted_image

def crop(image: list[list[tuple[int, int, int]]],
         x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    """
    Crea una nueva imagen recortada a partir de la imagen original.

    Args:
    - image: Lista de listas que representa la imagen original.
    - x: Coordenada x de la esquina superior izquierda del rectángulo a recortar.
    - y: Coordenada y de la esquina superior izquierda del rectángulo a recortar.
    - width: Ancho del rectángulo a recortar.
    - height: Altura del rectángulo a recortar.

    Returns:
    - La imagen recortada.
    """

    cropped_image = []
    for row in image[y:y+height]:
        cropped_image.append(row[x:x+width])
    return cropped_image

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """
    Convierte una imagen en escala de grises.

    Args:
    - image: Lista de listas que representa la imagen original.

    Returns:
    - La imagen convertida en escala de grises.
    """

    grayscale_image = []
    for row in image:
        grayscale_row = []
        for pixel in row:
            # Calcula la media de los valores RGB del pixel
            gray_value = sum(pixel) // 3
            # Asigna el mismo valor a los tres canales de color RGB
            grayscale_pixel = (gray_value, gray_value, gray_value)
            grayscale_row.append(grayscale_pixel)
        grayscale_image.append(grayscale_row)
    return grayscale_image

def filtro(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    """
    Aplica un filtro a cada pixel de la imagen multiplicando los valores RGB por los multiplicadores dados.
    Si el resultado es mayor que 255, se establece el valor como 255.

    Args:
    - image: Imagen a la que se aplicará el filtro.
    - r: Multiplicador para el componente rojo.
    - g: Multiplicador para el componente verde.
    - b: Multiplicador para el componente azul.

    Returns:
    - La imagen con el filtro aplicado.
    """
    filtered_image = []
    for row in image:
        filtered_row = []
        for pixel in row:
            red = min(int(pixel[0] * r), 255)
            green = min(int(pixel[1] * g), 255)
            blue = min(int(pixel[2] * b), 255)
            filtered_row.append((red, green, blue))
        filtered_image.append(filtered_row)
    return filtered_image

