#!/usr/bin/env python

from images import read_img, write_img, create_blank, size
from transforms import change_colors

def read_write_with_color_change():
    """Lee una imagen de un fichero, cambia algunos colores y la escribe"""

    pixels = read_img('cafe.jpg')

    # Color a cambiar: de rojo (255, 0, 0) a verde (0, 255, 0)
    color_to_change = (255, 0, 0)
    new_color = (0, 255, 0)

    changed_pixels = change_colors(pixels, color_to_change, new_color)

    write_img(changed_pixels, 'cafe_color_changed.png')

def main():
    read_write_with_color_change()

if __name__ == '__main__':
    main()
