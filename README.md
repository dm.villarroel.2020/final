# ENTREGA CONVOCATORIA ENERO
# DIEGO MARIO VILLARROEL VILLCA dm.villarroel.2020@alumnos.urjc.es
# Repositorio de plantilla: "Proyecto Final"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/practicas/final/final/README.md).

LINK AL VIDEO DE LA DEMOSTRACIÓN DE LOS PROGRAMAS:
https://youtu.be/eCKlVOznWRE?si=8mle_uw0X_Me049s

He realizado la parte de requisitos minimos y añadido algunas implementaciones para la parte opcional.
No he realizado push porque no me ha dejado, lo envío por correo y notifico previamente al profesor, si he realizado commit.

Para la parte opcional también he intentado hacer un menú para editar todo pero no lo he conseguido.